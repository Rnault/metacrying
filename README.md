# Metacrying - git repository

This repository contains all the material used to manipulate and analyse the data for this meta-analysis project of infant crying time across age groups and countries.
There are 8 different scripts corresponding to different stages of the analysis. By following them in order, one should transform the raw_datasheet.csv fole in usable form, reproduce all figures in the "plots" and "tables" folders and replicate our results.

### 01. Cleaning
This script gets rid of useless columns, renames others and manipulates the data to make it more human readable.
Takes the raw_datasheet.csv as input and outputs the "clean_datasheet.csv". 

### 02. Method Plots
This script generates the plots present in the methods part of the paper.

### 03. ANOVA
This scripts runs the ANOVA tests, generates ANOVA tables.
I also generates the figures in the results section (representing mean weighted values for crying, cry/fuss and total distress).

### 04. Mixed Effect: Cryfuss
This script runs the meta regression models for the cry/fuss crying category.
Also generates the associated forest plots.

### 05. Mixed Effect: Cry only
This script runs the meta regression models for the cry only crying category.
Also generates the associated forest plots.

### 06. Mixed Effect: Total distress
This script runs the meta regression models for the total distress crying category.
Also generates the associated forest plots.

### 07. Feeding Type
This script runs meta regression models to test for the impact of feeding type (breast vs. bottle feeding) on cry/fuss times at different age groups.

### 08. Publication Bias
This script test for publication bias and generate the associated funnel plots.
